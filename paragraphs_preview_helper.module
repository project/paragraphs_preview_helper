<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_entity_operation_alter().
 */
function paragraphs_preview_helper_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if (\Drupal::routeMatch()->getRouteName() == 'entity.paragraphs_type.collection') {
    $operations['configure-preview'] = [
      'title'  => t('Paragraph Preview'),
      'weight' => 100,
      'url'    => Url::fromRoute('paragraphs_preview_helper.manage', ['paragraphs_type' => $entity->id()]),
    ];
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function paragraphs_preview_helper_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {

  // Only apply changes to paragraph widgets.
  if (isset($context['widget']) && $context['widget']->getPluginId() == 'entity_reference_paragraphs') {
    $output = paragraphs_preview_helper_apply_override($context);
    if ($output) {

      // Override the paragraph summary
      $element['top']['paragraph_summary']['fields_info'] = [
        '#type'     => 'inline_template',
        '#template' => '<div class="paragraphs-collapsed-description">' . $output . '</div>',
      ];
    }
  }

  // Apply the changes to the paragraph previewer widgets.
  if (\Drupal::service('module_handler')->moduleExists('paragraphs_previewer')) {
    if (isset($context['widget']) && $context['widget']->getPluginId() == 'entity_reference_paragraphs_previewer') {
      $output = paragraphs_preview_helper_apply_override($context);
      if ($output) {
  
        // Override the paragraph summary
        $element['top']['paragraph_summary']['fields_info'] = [
          '#type'     => 'inline_template',
          '#template' => '<div class="paragraphs-collapsed-description">' . $output . '</div>',
        ];
      }
    }
  }
}

/**
 * Helper function to return the overridden paragraph summary when the edit mode is 'closed'
 * or 'preview'.
 */
function paragraphs_preview_helper_apply_override(&$context) {
  $output = FALSE;

  // Get the widget settings and only do the replacement if the edit mode is
  // 'closed' or 'preview'.
  $display_settings = $context['widget']->getSettings();
  if (isset($display_settings['edit_mode']) && $display_settings['edit_mode'] != 'open') {
    $settings = \Drupal::config('paragraphs_preview_helper.settings')->get('paragraph_types');
    if (isset($settings)) {

      // Get the entity
      $values = $context['items']->getValue();
      if (!empty($values[$context['delta']]['target_id'])) {
        $entity = \Drupal::entityTypeManager()->getStorage('paragraph')->load($values[$context['delta']]['target_id']);

        // Only modify the preview if it has a value in the config
        if (isset($entity) && isset($settings[$entity->bundle()])) {
          $output = \Drupal::token()->replace($settings[$entity->bundle()], ['paragraph' => $entity]);
        }
      }
    }
  }

  return $output;
}
